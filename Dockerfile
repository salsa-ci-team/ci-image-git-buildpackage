FROM debian:testing
LABEL maintainer="hans@eds.org"

ENV LANG=C.UTF-8 \
    DEBIAN_FRONTEND=noninteractive

# install the minimum needed for a standard git-buildpackage build
# that includes pristine-tar.
#
# * update-alternatives needs ../man1/
# * ca-certificates for fetching from HTTPS repos
RUN mkdir -p /usr/share/man/man1 \
	&& apt-get update \
	&& apt-get -qy upgrade \
	&& apt-get -qy dist-upgrade \
	&& export build_deps=' \
		build-essential \
		ca-certificates \
		fakeroot \
		git-buildpackage \
		lintian \
		pristine-tar' \
	&& apt-get -qy install --no-install-recommends $build_deps \
		autodep8 \
		autopkgtest \
		git \
	&& apt-get -qy autoremove --purge \
	&& apt-get clean \
	&& apt-mark auto $build_deps \
	&& rm -rf /var/lib/apt/lists/*

ADD overlay /

ENTRYPOINT ["/usr/bin/gitlab-ci-entrypoint"]
